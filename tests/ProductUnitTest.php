<?php

namespace App\Tests;

use App\Entity\Categories;
use App\Entity\Product;
use App\Entity\ReviewsProduct;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ProductUnitTest extends KernelTestCase
{
    public function testSomething(): void
    {
        $kernel = self::bootKernel();

        $this->assertSame('test', $kernel->getEnvironment());
        //$routerService = self::$container->get('router');
        //$myCustomService = self::$container->get(CustomService::class);
    }

    public function testIsTrue()
    {
        $product = new Product();
        //$datetime = new DateTime();
        $user = new User();
        $category = new Categories();
        $reviewsProducts =new ReviewsProduct();

        $product->setName('name')
        ->setPrice(20.20)
       // ->setCreatedAt($datetime)
        ->setImage('image')
        ->setDescription('description')
        ->setIsBestSeller(true)
        ->setIsFeatured(true)
        ->setIsNewArrival(true)
        ->setIsSpecialOffer(true)
        ->setMoreInformations("moreInformation")
        //->setQuantity(20)
        //->setReduction(20)
        //->setSlug("slug")
        ->addCategory($category)
        //->addFavori($user)
        ->addReviewsProduct($reviewsProducts);
        

        $this->assertTrue('name' === $product->getName());
        $this->assertTrue(20.20 == $product->getPrice());
        //$this->assertTrue($datetime === $product->getCreatedAt());
        $this->assertTrue(true === $product->getIsBestSeller());
        $this->assertTrue(true === $product->getIsNewArrival());
        $this->assertTrue(true === $product->getIsSpecialOffer());
        $this->assertTrue(true === $product->getIsFeatured());
        // $this->assertTrue('moreInformation' === $product->getMoreInformation());
        // $this->assertTrue(20 == $product->getQuantity());
        // $this->assertTrue(20 == $product->getReduction());
        // $this->assertTrue("slug" === $product->getSlug());

        $this->assertContains($category, $product->getCategory());
        //$this->assertContains($user, $product->getFavoris());
        $this->assertContains($reviewsProducts, $product->getReviewsProducts());
    }

    public function testIsFalse()
    {
        $product = new Product();
        //$datetime = new DateTime();
        $user = new User();
        $category = new Categories();
        $reviewsProducts =new ReviewsProduct();

        $product->setName('name')
        ->setPrice(20.20)
        //->setCreatedAt($datetime)
        ->setImage('image')
        ->setDescription('description')
        ->setIsBestSeller(true)
        ->setIsFeatured(true)
        ->setIsNewArrival(true)
        ->setIsSpecialOffer(true)
        // ->setMoreInformation("moreInformation")
        // ->setQuantity(20)
        // ->setReduction(20)
        // ->setSlug("slug")
        ->addCategory($category)
       // ->addFavori($user)
        ->addReviewsProduct($reviewsProducts);
        

        $this->assertFalse('false' === $product->getName());
        $this->assertFalse(0.0 == $product->getPrice());
        //$this->assertFalse(new DateTime() === $product->getCreatedAt());
        $this->assertFalse(false === $product->getIsBestSeller());
        $this->assertFalse(false === $product->getIsNewArrival());
        $this->assertFalse(false === $product->getIsSpecialOffer());
        $this->assertFalse(false === $product->getIsFeatured());
        // $this->assertFalse('false' === $product->getMoreInformation());
        // $this->assertFalse(0 == $product->getQuantity());
        // $this->assertFalse(0 == $product->getReduction());
        // $this->assertFalse("false" === $product->getSlug());

        $this->assertNotContains(new Categories(), $product->getCategory());
        //$this->assertNotContains(new User(), $product->getFavoris());
        $this->assertNotContains(new ReviewsProduct(), $product->getReviewsProducts());
    }

    public function testIsEmpty()
    {
        $product = new product();

        $this->assertEmpty($product->getName());
        $this->assertEmpty($product->getPrice());
        //$this->assertEmpty($product->getCreatedAt());
        $this->assertEmpty($product->getIsBestSeller());
        $this->assertEmpty($product->getIsNewArrival());
        $this->assertEmpty($product->getIsSpecialOffer());
        $this->assertEmpty($product->getIsFeatured());
        // $this->assertEmpty($product->getMoreInformation());
        // $this->assertEmpty($product->getQuantity());
        // $this->assertEmpty($product->getReduction());
        // $this->assertEmpty($product->getSlug());
        $this->assertEmpty($product->getCategory());
        // $this->assertEmpty($product->getFavoris());
        $this->assertEmpty($product->getReviewsProducts());
    }
}
