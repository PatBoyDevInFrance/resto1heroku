<?php

namespace App\Tests;

use App\Entity\Address;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class AddressTest extends TestCase
{
    public function testIsTrue()
    {
        $address = new Address();
        $user = new User();

        $address->setAddress('rue des')
        ->setCampany('b')
        ->setCity('pays')
        ->setCodePostal(0000)
        ->setComplement('abcd')
        ->setCountry('ville')
        ->setFullname('fullname')
        ->setPhone(0000)
        ->setUser($user);

        $this->assertTrue('rue des' === $address->getAddress());
        $this->assertTrue('b' === $address->getCampany());
        $this->assertTrue('pays' === $address->getCity());
        $this->assertTrue(0000 === $address->getCodePostal());
        $this->assertTrue('abcd' === $address->getComplement());
        $this->assertTrue('ville' === $address->getCountry());
        $this->assertTrue('fullname' === $address->getFullname());
        $this->assertTrue(0000 === $address->getPhone());
        $this->assertTrue($address->getUser() === $user);
    }

    public function testIsFalse()
    {
        $address = new Address();
        $user = new User();

        $address->setAddress('rue ')
        ->setCampany('c')
        ->setCity('paysa')
        ->setCodePostal(0001)
        ->setComplement('abcde')
        ->setCountry('village')
        ->setFullname('fullnames')
        ->setPhone(00001)
        ->setUser($user);

        $this->assertFalse('rue des' === $address->getAddress());
        $this->assertFalse('b' === $address->getCampany());
        $this->assertFalse('pays' === $address->getCity());
        $this->assertFalse(0000 === $address->getCodePostal());
        $this->assertFalse('abcd' === $address->getComplement());
        $this->assertFalse('ville' === $address->getCountry());
        $this->assertFalse('fullname' === $address->getFullname());
        $this->assertFalse(0000 === $address->getPhone());
        $this->assertFalse($address->getUser() === new User());
    }

    public function testIsEmpty()
    {
        $address = new Address();

        $this->assertEmpty($address->getAddress());
        $this->assertEmpty($address->getCampany());
        $this->assertEmpty($address->getCity());
        $this->assertEmpty($address->getCodePostal());
        $this->assertEmpty($address->getComplement());
        $this->assertEmpty($address->getCountry());
        $this->assertEmpty($address->getFullname());
        $this->assertEmpty($address->getPhone());
        $this->assertEmpty($address->getUser());
    }
}
