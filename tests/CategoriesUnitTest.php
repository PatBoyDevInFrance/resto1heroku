<?php

namespace App\Tests;

use App\Entity\Categories;
use App\Entity\Product;
use PHPUnit\Framework\TestCase;

class CategoriesUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $categories = new Categories();
        $product = new Product();

        $categories->setName('name')
        ->setDescription('description')
        ->addProduct($product);

        $this->assertTrue('name' === $categories->getName());
        $this->assertTrue('description' === $categories->getDescription());
        $this->assertContains($product, $categories->getProducts());
    }

    public function testIsFalse()
    {
        $categories = new Categories();
        $product = new Product();

        $categories->setName('name')
        ->setDescription('description')
        ->addProduct($product);

        $this->assertFalse('false' === $categories->getName());
        $this->assertFalse('false' === $categories->getDescription());
        $this->assertNotContains(new Product(), $categories->getProducts());
    }

    public function testIsEmpty()
    {
        $categories = new Categories();

        $this->assertEmpty($categories->getName());
        $this->assertEmpty($categories->getDescription());
    }
}
