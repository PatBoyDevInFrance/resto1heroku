<?php

namespace App\Tests;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserUnitTest extends KernelTestCase
{
    public function testIsTrue()
    {
        $user = new User();
        // $address = new Address();
        // $cart = new Cart();
        // $favori = new Product();
        // $order = new Order();

        $user->setEmail('true@test.com')
        ->setLastname('prenom')
        ->setFirstname('nom')
        ->setUsername('username')
        ->setIsVerified(true)
        ->setRoles(['ROLE_USER'])
        ->setPassword('password');
        // ->addAddress($address)
        // ->addCart($cart)
        // ->addFavori($favori)
        // ->addOrder($order);

        self::bootKernel();
        $error = self::$container->get('validator')->validate($user);
        $this->assertCount(0, $error);

        $this->assertTrue('true@test.com' === $user->getEmail());
        $this->assertTrue(['ROLE_USER'] === $user->getRoles());
        $this->assertTrue('prenom' === $user->getLastname());
        $this->assertTrue('nom' === $user->getFirstname());
        $this->assertTrue('password' === $user->getPassword());
        // $this->assertTrue('username' === $user->getUsername());
        // $this->assertContains($address, $user->getAddresses());
        // $this->assertContains($cart, $user->getCarts());
        // $this->assertContains($favori, $user->getFavoris());
        // $this->assertContains($order, $user->getOrders());
    }

    public function testIsFalse()
    {
        $user = new User();

        $user->setEmail('true@test.com')
        ->setLastname('prenom')
        ->setFirstname('nom')

        ->setUsername('username')
        ->setRoles(['ROLE_USER'])
        ->setPassword('password');

        $this->assertFalse('false@test.com' === $user->getEmail());

        $this->assertFalse(['ROLE_'] === $user->getRoles());
        $this->assertFalse('false' === $user->getLastname());
        $this->assertFalse('false' === $user->getFirstname());
        $this->assertFalse('false' === $user->getUsername());
        $this->assertFalse('false' === $user->getPassword());

        // self::bootKernel();
        // $error = self::$container->get('validator')->validate($user);
        // $this->assertCount(0, $error);
    }

    public function testIsEmpty()
    {
        $user = new User();

        $this->assertEmpty($user->getEmail());
        //$this->assertEmpty($user->getRoles());
        $this->assertEmpty($user->getLastname());
        $this->assertEmpty($user->getFirstname());
        $this->assertEmpty($user->getUsername());
        // $this->assertEmpty($user->getPassword());
    }
}
