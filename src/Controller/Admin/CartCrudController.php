<?php

namespace App\Controller\Admin;

use App\Entity\Cart;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CartCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Cart::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('user.FullName', 'Client'),
            TextField::new('CarrierName', 'Carrier Name'),
            MoneyField::new('CarrierPrice', 'Shippining')->setCurrency('EUR'),
            MoneyField::new('subTotalHT', 'Subtotal HT')->setCurrency('EUR'),
            MoneyField::new('taxe', 'TVA')->setCurrency('EUR'),
            MoneyField::new('subTotalTTC', 'Subtotal TTC')->setCurrency('EUR'),
            BooleanField::new('isPaid')
        ];
    }
    
}
