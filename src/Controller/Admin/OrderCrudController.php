<?php

namespace App\Controller\Admin;

use App\Entity\Order;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class OrderCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Order::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud->setDefaultSort(['id'=>'DESC']);
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('user.FullName', 'Client'),
            TextField::new('user.LastName', 'Nom'),
            TextField::new('user.FirstName', 'Prénom'),
            TextField::new('user.Email', 'email'),
            DateTimeField::new('createdAt','passé le'),
            TextField::new('CarrierName', 'Carrier Name'),  
            MoneyField::new('subTotalTTC', 'Subtotal TTC')->setCurrency('EUR'),
            BooleanField::new('isPaid', 'payer')
        ];
    }
}
